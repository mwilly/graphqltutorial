const { GraphQLServer } = require('graphql-yoga')

let links = [{
    id: 'link-0',
    url: 'www.howtographql.com',
    description: 'Fullstack tutorial for GrpahQL'
}]

let idCount = links.length;

const resolvers = {
    Query: {
        info: () => `This is the API of a Hackernews Clone`,
        link: (root, args) => { const link = links.filter(link => link.id === `link-${args.id}`);
            return link},
        feed: () => links, 
    },
    Mutation: {
        post: (root, args) => {
            const link = {
                id: `link-${idCount++}`,
                description: args.description,
                url: args.url
            }
            links.push(link);
            return link;
        },
        updateLink: (root, args) => {
            const id = `link-${args.id}`
            const newLink = {
               id, 
               description: args.description,
               url: args.url
            };
            links = links.map((link) => {
                if(link.id === id) {
                    link = newLink;
                }
                return link;
            })
            return newLink
        },
        deleteLink: (root, args) => {
            const id = `link-${args.id}`;
            removedLink = links.filter(link => link.id === id);  
            if(removedLink != []) {
                links = links.filter(link => link.id !== id);
            }
            return removedLink;
        }
        
    },
}

const server = new GraphQLServer({
    typeDefs: './src/schema.graphql',
    resolvers
})
server.start(() => console.log(`Server is running on http://localhost:4000`))
